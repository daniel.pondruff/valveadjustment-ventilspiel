# Valveadjustment/Ventilspiel Einstellung


Version 0.2

I have created a small tool in Excel/Numbers to facilitate the calculation of the valves.

Todo:
- Suggestions for Shim
- Bar chart of the valve adjustment

Screenshot:
![Screenshot](Screenshot.png)

NOTE:
I created the table with Numbers. Numbers is an Excel alternative from Apple for MacOS. 
However, I also exported the table as an Excel file but did not test it. If you encounter any problems here, please contact me